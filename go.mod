module spider

require (
	github.com/PuerkitoBio/goquery v1.4.1
	github.com/andybalholm/cascadia v1.0.0 // indirect; direct
	github.com/cnjack/echo-binder v0.0.0-20170922053921-58bd3f68d434
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/denisenkom/go-mssqldb v0.0.0-20180901172138-1eb28afdf9b6 // indirect; direct
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/erikstmartin/go-testdb v0.0.0-20160219214506-8d10e4a1bae5 // indirect; direct
	github.com/go-playground/locales v0.12.1 // indirect; direct
	github.com/go-playground/universal-translator v0.16.0 // indirect; direct
	github.com/go-redis/redis v6.14.1+incompatible
	github.com/go-sql-driver/mysql v1.4.0 // indirect; direct
	github.com/go-yaml/yaml v2.1.0+incompatible
	github.com/jinzhu/gorm v1.9.1
	github.com/jinzhu/inflection v0.0.0-20180308033659-04140366298a // indirect; direct
	github.com/jinzhu/now v0.0.0-20180511015916-ed742868f2ae // indirect; direct
	github.com/kr/pretty v0.1.0 // indirect; direct
	github.com/labstack/echo v3.2.1+incompatible
	github.com/labstack/gommon v0.2.7 // indirect; direct
	github.com/lib/pq v1.0.0 // indirect
	github.com/mattn/go-colorable v0.0.9 // indirect; direct
	github.com/mattn/go-isatty v0.0.4 // indirect; direct
	github.com/mattn/go-sqlite3 v1.9.0 // indirect; direct
	github.com/microcosm-cc/bluemonday v1.0.1 // indirect; direct
	github.com/onsi/gomega v1.4.2 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect; direct
	github.com/stretchr/testify v1.2.2
	github.com/valyala/bytebufferpool v1.0.0 // indirect; direct
	github.com/valyala/fasttemplate v0.0.0-20170224212429-dcecefd839c4 // indirect; direct
	golang.org/x/crypto v0.0.0-20190211182817-74369b46fc67 // indirect
	golang.org/x/net v0.0.0-20180906233101-161cd47e91fd
	golang.org/x/text v0.3.0
	google.golang.org/appengine v1.1.0 // indirect; direct
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect; direct
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect; direct
	gopkg.in/go-playground/validator.v9 v9.21.0 // indirect; direct
)
